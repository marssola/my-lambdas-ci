'use strict';

export const handler = async (): Promise<any> => {
    try {
        return { body: 'MyLambda2' }
    } catch (error) {
        return {
            body: { error }
        }
    }
};