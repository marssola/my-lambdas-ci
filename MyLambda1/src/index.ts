'use strict';

export const handler = async (): Promise<any> => {
    try {
        return { body: 'MyLambda1' }
    } catch (error) {
        return {
            body: { error }
        }
    }
};