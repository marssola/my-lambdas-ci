'use strict';

export const handler = async (): Promise<any> => {
    try {
        return { body: 'MyLambda3' }
    } catch (error) {
        return {
            body: { error }
        }
    }
};